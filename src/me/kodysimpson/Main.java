package me.kodysimpson;

import java.lang.reflect.*;

public class Main {

    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException {

        //How to invoke constructors
        Class llamaClass = Llama.class;

        Constructor constructor = llamaClass.getConstructor(String.class, int.class, String.class, boolean.class);

        Llama llama = (Llama) constructor.newInstance("Bob", 2, "poop", false);

        System.out.println(llama);

        //invocation of the spit method using reflection
//        Method spitMethod = llamaClass.getDeclaredMethod("spit");
//        spitMethod.invoke(llama);

        //grab the method at runtime and then invoke it
//        Method[] methods = llamaClass.getDeclaredMethods();
//        for(Method method : methods){
//            if (method.getName().equalsIgnoreCase("spit") || method.getName().equalsIgnoreCase("fart")){
//                method.setAccessible(true);
//                method.invoke(llama);
//            }
//        }

        Method[] methods = llamaClass.getDeclaredMethods();
        for(Method method : methods){

            //Parameter[] parameters = method.getParameters();

            if (method.getParameterCount() == 0){
                method.setAccessible(true);
                method.invoke(llama);
            }


        }

        //change the value of a field
        Field eyeField = llamaClass.getDeclaredField("eyeCount");
        eyeField.setAccessible(true);
        eyeField.set(llama, 30);

        System.out.println(llama);



    }
}
